.model small
.stack 100h

.data
    countL db 0 
    countN db 0
    countO db 0
    msgRtn db 13, 10, '$'
    msgIpt db 'input a line: $'
    msgLet db 'Letter: $'
    msgNum db 'Number: $'
    msgEtc db 'Other: $'
    iptPtr db 80
    acSize db ?
    strPtr db 80 dup (?)

.code

endl proc near
    push ax
    push dx
    mov  ah, 9
    lea  dx, msgRtn
    int  21h
    pop  dx
    pop  ax
    ret
endl endp

printB proc near ;打印cl里面的数字
    push ax
    push dx
    mov  ah, 0 ;被除数
    mov  al, cl;被除数
    mov  dl, 10;除数
    div  dl    ;ax除以10，商在al中，余数在ah中 
    mov  dx, ax;商在（十位）dl中，余数（个位）在dh中 
    cmp  dl, 0
    je   lessThanX
    add  dl, 48
    mov  ah, 2
    int  21h
    lessThanX:
    mov  dl, dh
    add  dl, 48
    mov  ah, 2
    int  21h

    pop  dx
    pop  ax
    ret
printB endp

.startup
main proc far
    ;定位附加段
    mov ax, ds
    mov es, ax

    jmp begin     ;调试使用，可以不改变程序命令地址实现跳过步骤
    begin:
    mov ah, 9
    lea dx, msgIpt
    int 21h

    mov ah, 10
    lea dx, iptPtr
    int 21h
    call endl


    lea si, strPtr
    mov bx, 0
    mov al, acSize
    check:
    mov ch, [bx][si];因为要多次比较，所以使用寄存器
    cmp ch, 48
    jl  other;0-47   
    cmp ch, 58
    jl  number;48-57
    cmp ch, 65
    jl  other;58-64   
    cmp ch, 91
    jl  letter;65-90
    cmp ch, 97
    jl  other;91-96
    cmp ch, 123
    jl  letter;97-122
    jmp other
    number:
    inc countN
    jmp iterate
    letter:
    inc countL
    jmp iterate
    other:
    inc countO
    jmp iterate
    iterate:
    inc bx
    cmp bl, al;al存的是实际长度
    jne check

    mov ah, 9
    lea dx, msgLet
    int 21h
    mov cl, countL
    call printB
    call endl

    mov ah, 9
    lea dx, msgNum
    int 21h
    mov cl, countN
    call printB
    call endl

    mov ah, 9
    lea dx, msgEtc
    int 21h
    mov cl, countO
    call printB
    call endl
    
    .exit 0

main endp
end