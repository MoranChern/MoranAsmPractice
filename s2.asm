.model small
.stack 100h
.data
    ptrStr db 80
    lenStr db ?
    bufStr db 80 DUP(?)
    ptrKey db 80
    lenKey db ?
    bufKey db 80 DUP(?)    
    msgRtn db 13, 10
    msgIpS db 'Enter sentence: $'
    msgIpK db 'Enter keyword: $'
    msgMa1 db 'Match at location: $'
    msgMa2 db 'h of the sentence. $'
    msgNoM db 'No match! $'
.code

endl       proc near;0000    输出回车
    push ax
    push dx
    mov  ah, 9
    lea  dx, msgRtn
    int  21h
    pop  dx
    pop  ax
    ret
endl       endp

printHex   proc near;print dl
    push ax
    cmp dl, 0
    jl  NaN
    cmp dl, 10
    jl  lX
    cmp dl, 16
    jl  gX
    jmp NaN
    lX:
    add dl, 48
    jmp printCh
    gX:
    add dl, 87
    jmp printCh
    NaN:
    mov dl, '?'
    jmp printCh
    printCh:
    mov ah, 2
    int 21h
    pop ax
    ret
printHex   endp

.startup
main proc far
    mov ax, ds
    mov es, ax

    jmp init
init:
    mov ah, 9
    lea dx, msgIpK
    int 21h
    mov ah, 10
    lea dx, ptrKey
    int 21h
    call endl
    
input_sentence:
    mov ah, 9
    lea dx, msgIpS
    int 21h
    mov ah, 10
    lea dx, ptrStr
    int 21h
    call endl

Find_match:
    lea si, bufKey
    mov di, 0
    
    mov ch, 0
    mov cl, lenKey;cx存key长度

    mov bh, 0 
    mov bl, lenStr
    mov di, bx    ;di存句子有效长度

    sub di, cx
    search:
    cmp di, 0
    jl  No_Match
    mov bx, cx
    dec bx     ;从key末尾开始比较
        cmpKey:
        cmp bx, 0
        jl  matched
        mov al, byte ptr bufStr[bx][di]
        cmp al, byte ptr [bx][si]
        jne continueSearch
        dec bx
        jmp cmpKey
    continueSearch:
    dec di
    jmp search


matched:;输出di 16进制
    mov ah, 9
    lea dx, msgMa1
    int 21h
    
    inc di

    mov dx, di
    cmp di, 16
    jl Print2ndCh
    mov cl, 4
    shr dl, cl
    call printHex
    
    mov dx, di
    and dl, 15
    Print2ndCh:
    call printHex

    mov ah, 9
    lea dx, msgMa2
    int 21h
    call endl
    jmp input_sentence

No_Match:
    mov ah, 9
    lea dx, msgNoM
    int 21h
    nop
    call endl
    jmp input_sentence

exit:;正常情况下，不会用到
    .exit 0

main endp
end