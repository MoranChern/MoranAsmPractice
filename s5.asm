.model small
.stack 100h
.data
    ;      -2 0105晚
    buff   label word    ;
    buff1  dw 0,0,0,0,0,1;0006
    buff2  dw 0,0,0,0,0,1;0012-18
    buff3  dw 0,0,0,0,0,2;001e-30
    prtBuf db 24 dup (?) ;002a-42  用来放输出的数字
    bufEnd db '$'        ;0042-66
    iptPtr db 3          ;0043-67
    iptLen db ?          ;0044-68
    iptBuf db 3 dup (?)  ;0045-69
    msgIpt db 'Input a number (in [1,99]): $' 
    msgPrt db 'Answer is: $'
    msgNaN db 'Illegal input! $'
    msgRtn db 13, 10, '$'
    
.code

endl     proc near;0000    输出回车
    push ax
    push dx
    mov  ah, 9
    lea  dx, msgRtn
    int  21h
    pop  dx
    pop  ax
    ret
endl     endp

strCpy   proc near;000d    [di]<-[si],距离ax
    push cx
    push bx
    mov bx, 0
    cpy:
    mov ch, byte ptr[bx][si]
    mov byte ptr[bx][di], ch
    inc bx
    cmp bx, ax
    jne cpy 
    pop bx
    pop cx
    ret
strCpy   endp

numToStr proc near;[di]<-(ax)
    push cx
    push bx
    push dx
    mov cx, 10;除数
    mov bx, 4-1
    trans:
    mov dx, 0 ;被除数高16位
    div cx    ;AX存储除法操作的商，DX存储除法操作的余数
    add dx, 48;转换为ASCII
    mov [bx][di], dl
    dec bx
    cmp bx, 0
    jnl trans
    pop dx
    pop bx
    pop cx
    ret
numToStr endp

.startup
main proc far
    ;定位附加段
    mov ax, ds
    mov es, ax

    jmp begin      ;调试使用的跳转指令

begin:
    mov ah, 9
    lea dx, msgIpt
    int 21h
    ;输入
    mov ah, 10
    lea dx, iptPtr
    int 21h
    call endl

get_input:
    mov cl, 0
    lea bx, iptBuf    
    cmp iptLen, 2;输入大于99
    jg  NaN
    cmp iptLen, 1
    je  inputOneDig
    inputTwoDig:
    mov al, [bx]
    sub al, 48
    cmp al, 0  ;非数字
    jl  NaN
    cmp al, 9  ;非数字
    jg  NaN
    mov ch, 10
    mul ch       ;十位从此存在al
    mov cl, al   ;部分结果在cl
    inc bx
    inputOneDig:
    mov ch, [bx]
    sub ch, 48
    cmp ch, 0  ;非数字
    jl  NaN
    cmp ch, 9  ;非数字
    jg  NaN
    add cl, ch ;结果拼合，存在cl
    cmp cl, 1  ;0输入
    jl  NaN
    cmp cl, 3
    jl  intput1
    cmp cl, 4
    jl  intput3
    sub cl, 3
    mov ch, 0;现在cx就是循环次数

caculate:
    ;相加部分 先移动，再相加
    iterate:
    ;移动
    mov ax, 12
    lea di, buff1
    lea si, buff2
    call strCpy
    lea di, buff2
    lea si, buff3
    call strCpy

    mov bx, 12
    mov dx, 0  ;实现进位 
    lea di, buff3
    dadc:
    sub bx, 2     ;迭代
    lea si, buff2
    mov ax, [bx][si]
    lea si, buff1
    add ax, [bx][si]
    add ax, dx ;实现进位
    mov dx, 0  ;实现进位
    cmp ax, 10000
    jl  noIncome
    sub ax, 10000
    mov dx, 1  ;实现进位
    noIncome:
    mov [bx][di], ax
    cmp bx, 0
    jne dadc
    dec cx
    jnz iterate

transfer_num_to_str:
    ;2变10
    lea si, buff3
    lea di, prtBuf
    add di, 20
    mov bx, 10
    mov cx, 6
    transfer:
    mov ax, [bx][si]
    ;二变十
    call numToStr
    ;二变十结束
    sub di, 4
    sub bx, 2
    dec cx
    jnz transfer


find_print_pointer:
    ;打印输出提示
    lea dx, msgPrt
    mov ah, 9
    int 21h
    ;找输出指针
    lea dx, prtBuf
    findPtr:
    mov bx, dx
    cmp byte ptr[bx], '0'
    jne print
    inc dx
    jmp findPtr
    mov bufEnd, '$'
    print:
    mov ah, 9
    int 21h
    call endl

    .exit 0

    intput1:
    mov dl, '1'
    mov ah, 2
    int 21h
    call endl
    .exit 0
    intput3:
    mov dl, '2'
    mov ah, 2
    int 21h
    call endl
    .exit 0
    NaN:
    mov ah, 9
    lea dx, msgNaN
    int 21h
    call endl
    .exit 1
main endp
end