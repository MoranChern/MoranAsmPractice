.model small
.stack 100h ;问一下老师局部性原理
.data
    ;
    iptNam db 21
    lenNam db ?
    bufNam db 21 dup (?), '$'
    bufTel db 9 dup (?)
    msgRtn db 13, 10, '$'
    msgCfm db '(Y/N) $'
    msgRCf db 13, 10, 'Input Again! (Y/N) $'
	msgINa db 'Input name: $'
	msgITl db 'Input a telephone number: $'
	msgMoT db 'Do you want to input another number? $'
	msgSeT db 'Do you want to seach a number? $'
	msgHea db '  Name                 Tel.',13,10,'$'
	msgNoF db 'Not find $'
    msgErr db 13, 10, 'Error! ', 13, 10, '$'
    lnkTab label byte
    lnkHdN db 32 dup (?)
           db 50 dup (20 dup(?),'$', 8 dup(?),'$', ?, ?  )
    indTab dw 0 ;入表指针di 最大为1568
    ;bx偏移指针的值  0-name;+21-tel;+30 next
.code

endl       proc near;0000    输出回车
    push ax
    push dx
    mov  ah, 9
    lea  dx, msgRtn
    int  21h
    pop  dx
    pop  ax
    ret
endl       endp

strCpy     proc near;    [di]<-[si],距离ax
    push cx
    push bx
    mov bx, 0
    cpy:
    mov ch, byte ptr[bx][si]
    mov byte ptr[bx][di], ch
    inc bx
    cmp bx, ax
    jne cpy 
    pop bx
    pop cx
    ret
strCpy     endp

comfirm    proc near; cmp 1,0(yes) cmp 0,0(no)
    push ax
    push dx
    mov ah, 9
    lea dx, msgCfm
    int 21h
    mov ah, 1
    int 21h
    jmp comfirmCheck    
    reComfirm:
    mov ah, 9
    lea dx, msgRCf
    int 21h
    mov ah, 1
    int 21h
    comfirmCheck:
    cmp al, 'y'
    je  comfirmYes
    cmp al, 'Y'
    je  comfirmYes
    cmp al, 'n'
    je  comfirmNo
    cmp al, 'N'
    je  comfirmNo
    jmp reComfirm
    comfirmYes:
    mov al, 1
    jmp endComfirm
    comfirmNo:
    mov al, 0
    jmp endComfirm
    endComfirm:
    call endl
    cmp al, 0
    pop dx
    pop ax
    ret
comfirm    endp

inputName  proc near
    push ax
    push bx
    push dx
    mov ah, 9
    lea dx, msgINa
    int 21h
    mov ah, 10
    lea dx, iptNam
    int 21h
    call endl
    lea bx, bufNam
    mov dx, 0
    add dl, lenNam
    add bx, dx
    mov byte ptr[bx], '$'
    pop dx
    pop bx
    pop ax
    ret
inputName  endp

inputTel   proc near
    push ax
    push bx
    push dx
    push di
    mov ah, 9
    lea dx, msgITl
    int 21h
    lea di, bufTel
    mov bx, 0
    mov ah, 1
    getANumber:
    int 21h
    cmp al, 48
    jl  error
    cmp al, 57
    jg  error
    mov byte ptr[bx][di], al
    inc bx
    cmp bx, 8
    jl  getANumber
    mov byte ptr[bx][di], '$'
    pop di
    pop dx
    pop bx
    pop ax
    ret
inputTel   endp

cmpName    proc near ;cmp [di], [si]
    push ax
    push bx
    mov bx, -1
    cmpByte:    
    inc bx    
    cmp bx, 20
    je  endCmp
    mov al, byte ptr [bx][di]
    cmp al, byte ptr [bx][si]
    jne endCmp
    cmp byte ptr [bx][di], '$'
    je  endCmp
    jmp cmpByte
    endCmp:
    pop bx
    pop ax
    ret
cmpName    endp

.startup
main proc far  
    ;定位附加段
    mov ax, ds
    mov es, ax

    jmp init
init:
    ;输入
    call inputName
    call inputTel
    call endl
    ;创建首元和头节点
    mov bx, 30
    mov di, 0
    mov word ptr lnkTab[bx][di], 32;头节点
    add di, 32
    mov word ptr lnkTab[bx][di], -1;首元节点
    ;复制数据
    ;复制名字
    mov bx, 0
    mov ah, 0
    mov al, lenNam
    inc al         ;把'$'存进去
    mov indTab, di;32
    lea di, lnkTab[bx][di]
    lea si, bufNam
    call strCpy
    ;复制电话
    mov di, indTab;32
    mov bx, 21
    mov ax, 9
    lea di, lnkTab[bx][di]
    lea si, bufTel
    call strCpy
    add indTab, 32
    jmp askAddAnother

new_Tel:  
    ;是否存满
    cmp indTab, 1632
    je  error
    ;输入
    call inputName
    call inputTel
    call endl
    ;存储名字
    mov di, indTab
    mov bx, 0
    mov ah, 0
    mov al, lenNam
    inc al         ;把'$'存进去
    lea di, lnkTab[bx][di]
    lea si, bufNam
    call strCpy
    ;存储名字
    mov di, indTab
    mov bx, 21
    mov ax, 9
    lea di, lnkTab[bx][di]
    lea si, bufTel
    call strCpy
    ;排序
    ;dx存放上一个节点地址;cx存放这一节点
    mov dx, 0
    sortLink:
    mov di, dx
    ;看上一个节点是不是链尾
    mov bx, 30
    mov cx, word ptr lnkTab[bx][di]
    cmp cx, -1
    jne notTail
    lnkTail: 
    mov si, indTab
    mov word ptr lnkTab[bx][di], si
    mov word ptr lnkTab[bx][si], -1
    add si, 32
    mov indTab, si
    jmp askAddAnother
    notTail:    
    ;和【这一个节点】比大小，jng, 就插在【这一个节点】之前  
    mov bx, 0;这时候di还是上一节点指针
    mov di, cx
    lea si, lnkTab[bx][di]
    mov di, indTab
    lea di, lnkTab[bx][di]
    call cmpName
    jng insertHere
    insertHere:
    mov bx, 30
    mov di, dx
    mov si, indTab    
    mov cx, word ptr lnkTab[bx][di];缓存[这个节点]    
    mov word ptr lnkTab[bx][di], si
    mov word ptr lnkTab[bx][si], cx
    add si, 32
    mov indTab, si
    jmp askAddAnother
    notInsertHere:
    mov bx, 30
    mov di, dx
    mov dx, word ptr lnkTab[bx][di]
    jmp sortLink    

    askAddAnother:
    mov ah, 9
    lea dx, msgMoT
    int 21h
    call comfirm ;; cmp 1,0(yes) cmp 0,0(no)
    jne new_Tel
    jmp askSearchAnother

search_tel:
    call inputName
    lea si, bufNam
    mov di, 0   ;头节点
    mov bx, 30  ;next指针
    mov dx, word ptr lnkTab[bx][di]
    findTheName:
    mov bx, 0   ;name
    mov di, dx  ;节点
    lea di, lnkTab[bx][di];节点name
    call cmpName
    je   print_Result
    ;迭代
    mov bx, 30
    mov di, dx
    mov dx, word ptr lnkTab[bx][di]
    cmp dx, -1
    je  notFind
    jmp findTheName

    notFind:
    mov ah, 9
    lea dx, msgNoF
    int 21h
    call endl
    jmp askSearchAnother

print_Result:;dx存放输出项目的偏移指针
    mov di, dx
    
    lea dx, msgHea
    mov ah, 9
    int 21h

    mov bx, 0
    lea dx, lnkTab[bx][di]
    int 21h

    mov cx, 21
    sub cl, lenNam
    mov dl, ' '
    mov ah, 2
    printSpace:
    int 21h
    dec cx
    cmp cx, 0
    jne printSpace

    mov ah, 9
    mov bx, 21
    lea dx, lnkTab[bx][di]
    int 21h
    call endl

    askSearchAnother:
    mov ah, 9
    lea dx, msgSeT
    int 21h
    call comfirm ;; cmp 1,0(yes) cmp 0,0(no)
    jne search_tel  

    call endl
    .exit 0
    error:
    mov ah, 9
    lea dx, msgErr
    int 21h
    .exit 1

main endp
end